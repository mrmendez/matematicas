package com;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
public class LeerArchivo {
    public Map extraeDatos(String nombreArchivo) throws FileNotFoundException, IOException{
        Map datos = null;
   /*PASO 1*/        FileInputStream fis = new FileInputStream(nombreArchivo);
   /*PASO 2*/        DataInputStream dis = new DataInputStream(fis);
   /*PASO 3*/        BufferedReader br = new BufferedReader(new 
                                             InputStreamReader(dis));
   String tmp="";  
   datos = new HashMap();
   StringTokenizer st = null;
   while((tmp=br.readLine()) != null){
           st= new StringTokenizer(tmp,",");
           while(st.hasMoreTokens()){
           datos.put((Double.parseDouble(st.nextToken())), (Double.parseDouble(st.nextToken())));
           }
   }
   return datos;
    }
}
